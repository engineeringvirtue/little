EESchema Schematic File Version 4
LIBS:pcb-cache
EELAYER 29 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 3 3
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Text Label 2300 5250 2    50   ~ 0
NRESET
Wire Wire Line
	2300 5250 2400 5250
Wire Wire Line
	5900 3100 5900 3350
Wire Wire Line
	6000 3100 5900 3100
$Comp
L Device:R R?
U 1 1 5D795385
P 6150 3100
AR Path="/5D795385" Ref="R?"  Part="1" 
AR Path="/5D76ED37/5D795385" Ref="R?"  Part="1" 
F 0 "R?" V 5943 3100 50  0000 C CNN
F 1 "470" V 6034 3100 50  0000 C CNN
F 2 "" V 6080 3100 50  0001 C CNN
F 3 "~" H 6150 3100 50  0001 C CNN
	1    6150 3100
	0    1    1    0   
$EndComp
$Comp
L Device:R R?
U 1 1 5D79538B
P 6150 3450
AR Path="/5D79538B" Ref="R?"  Part="1" 
AR Path="/5D76ED37/5D79538B" Ref="R?"  Part="1" 
F 0 "R?" V 5943 3450 50  0000 C CNN
F 1 "470" V 6034 3450 50  0000 C CNN
F 2 "" V 6080 3450 50  0001 C CNN
F 3 "~" H 6150 3450 50  0001 C CNN
	1    6150 3450
	0    1    1    0   
$EndComp
$Comp
L Device:R R?
U 1 1 5D795391
P 2750 5600
AR Path="/5D795391" Ref="R?"  Part="1" 
AR Path="/5D76ED37/5D795391" Ref="R?"  Part="1" 
F 0 "R?" V 2543 5600 50  0000 C CNN
F 1 "470" V 2634 5600 50  0000 C CNN
F 2 "" V 2680 5600 50  0001 C CNN
F 3 "~" H 2750 5600 50  0001 C CNN
	1    2750 5600
	0    1    1    0   
$EndComp
Wire Wire Line
	2900 5600 2900 5850
Wire Wire Line
	2500 5600 2600 5600
Text Notes 2800 5000 2    50   ~ 0
im going to pray that this is enough
Connection ~ 2900 5250
Wire Wire Line
	3050 5250 3050 5750
Wire Wire Line
	2900 5250 3050 5250
Wire Wire Line
	2700 5250 2900 5250
$Comp
L Device:R R?
U 1 1 5D79539E
P 2900 5100
AR Path="/5D79539E" Ref="R?"  Part="1" 
AR Path="/5D76ED37/5D79539E" Ref="R?"  Part="1" 
F 0 "R?" H 2970 5146 50  0000 L CNN
F 1 "100K" H 2970 5055 50  0000 L CNN
F 2 "" V 2830 5100 50  0001 C CNN
F 3 "~" H 2900 5100 50  0001 C CNN
	1    2900 5100
	1    0    0    -1  
$EndComp
$Comp
L power:+3.7V #PWR?
U 1 1 5D7953A4
P 2900 4950
AR Path="/5D7953A4" Ref="#PWR?"  Part="1" 
AR Path="/5D76ED37/5D7953A4" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 2900 4800 50  0001 C CNN
F 1 "+3.7V" H 2915 5123 50  0000 C CNN
F 2 "" H 2900 4950 50  0001 C CNN
F 3 "" H 2900 4950 50  0001 C CNN
	1    2900 4950
	1    0    0    -1  
$EndComp
$Comp
L Device:R R?
U 1 1 5D7953AA
P 2550 5250
AR Path="/5D7953AA" Ref="R?"  Part="1" 
AR Path="/5D76ED37/5D7953AA" Ref="R?"  Part="1" 
F 0 "R?" V 2343 5250 50  0000 C CNN
F 1 "470" V 2434 5250 50  0000 C CNN
F 2 "" V 2480 5250 50  0001 C CNN
F 3 "~" H 2550 5250 50  0001 C CNN
	1    2550 5250
	0    1    1    0   
$EndComp
Text Label 2500 5600 2    50   ~ 0
SWCLK
Wire Wire Line
	2500 5950 2600 5950
$Comp
L Device:R R?
U 1 1 5D7953B2
P 2750 5950
AR Path="/5D7953B2" Ref="R?"  Part="1" 
AR Path="/5D76ED37/5D7953B2" Ref="R?"  Part="1" 
F 0 "R?" V 2543 5950 50  0000 C CNN
F 1 "470" V 2634 5950 50  0000 C CNN
F 2 "" V 2680 5950 50  0001 C CNN
F 3 "~" H 2750 5950 50  0001 C CNN
	1    2750 5950
	0    1    1    0   
$EndComp
Text Label 2500 5950 2    50   ~ 0
SWDIO
Text Label 5950 2150 1    50   ~ 0
CHRG
$Comp
L Device:R R?
U 1 1 5D7953BA
P 4350 1100
AR Path="/5D7953BA" Ref="R?"  Part="1" 
AR Path="/5D76ED37/5D7953BA" Ref="R?"  Part="1" 
F 0 "R?" V 4143 1100 50  0000 C CNN
F 1 "100K" V 4234 1100 50  0000 C CNN
F 2 "Resistor_SMD:R_0402_1005Metric" H 4420 1009 50  0001 L CNN
F 3 "~" H 4350 1100 50  0001 C CNN
	1    4350 1100
	0    1    1    0   
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5D7953C0
P 4200 1100
AR Path="/5D7953C0" Ref="#PWR?"  Part="1" 
AR Path="/5D76ED37/5D7953C0" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 4200 850 50  0001 C CNN
F 1 "GND" H 4205 927 50  0000 C CNN
F 2 "" H 4200 1100 50  0001 C CNN
F 3 "" H 4200 1100 50  0001 C CNN
	1    4200 1100
	0    1    1    0   
$EndComp
$Comp
L Device:Crystal Y?
U 1 1 5D7953C6
P 2800 3650
AR Path="/5D7953C6" Ref="Y?"  Part="1" 
AR Path="/5D76ED37/5D7953C6" Ref="Y?"  Part="1" 
F 0 "Y?" H 2800 3918 50  0000 C CNN
F 1 "32MHz" H 2800 3827 50  0000 C CNN
F 2 "" H 2800 3650 50  0001 C CNN
F 3 "~" H 2800 3650 50  0001 C CNN
	1    2800 3650
	1    0    0    -1  
$EndComp
NoConn ~ 5800 4850
NoConn ~ 5800 4950
NoConn ~ 5800 5050
NoConn ~ 5800 5150
NoConn ~ 5800 2750
NoConn ~ 5800 5250
NoConn ~ 5800 2650
NoConn ~ 5800 5350
NoConn ~ 5800 5650
NoConn ~ 5800 5450
NoConn ~ 5800 5550
NoConn ~ 4100 1250
NoConn ~ 5800 2350
NoConn ~ 5800 2250
NoConn ~ 5800 2450
Wire Wire Line
	5800 2150 5950 2150
NoConn ~ 5800 2050
NoConn ~ 5800 2550
NoConn ~ 5800 1950
NoConn ~ 3200 2250
NoConn ~ 5800 4150
NoConn ~ 5800 4050
$Comp
L power:GND #PWR?
U 1 1 5D7953E2
P 4600 6850
AR Path="/5D7953E2" Ref="#PWR?"  Part="1" 
AR Path="/5D76ED37/5D7953E2" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 4600 6600 50  0001 C CNN
F 1 "GND" V 4605 6722 50  0000 R CNN
F 2 "" H 4600 6850 50  0001 C CNN
F 3 "" H 4600 6850 50  0001 C CNN
	1    4600 6850
	1    0    0    -1  
$EndComp
NoConn ~ 5800 1650
NoConn ~ 4200 1250
NoConn ~ 3900 1250
NoConn ~ 5800 1550
NoConn ~ 3700 1250
NoConn ~ 5800 4450
$Comp
L power:GND #PWR?
U 1 1 5D7953EE
P 4500 6850
AR Path="/5D7953EE" Ref="#PWR?"  Part="1" 
AR Path="/5D76ED37/5D7953EE" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 4500 6600 50  0001 C CNN
F 1 "GND" V 4505 6722 50  0000 R CNN
F 2 "" H 4500 6850 50  0001 C CNN
F 3 "" H 4500 6850 50  0001 C CNN
	1    4500 6850
	1    0    0    -1  
$EndComp
NoConn ~ 4000 1250
NoConn ~ 4900 1250
NoConn ~ 5800 5850
NoConn ~ 5800 5950
NoConn ~ 5800 6150
NoConn ~ 5800 1850
NoConn ~ 5800 4250
NoConn ~ 5800 2850
NoConn ~ 5800 6550
NoConn ~ 5800 6450
NoConn ~ 5800 4750
NoConn ~ 5800 3850
NoConn ~ 4700 1250
NoConn ~ 5800 3650
Wire Wire Line
	5800 3450 6000 3450
NoConn ~ 5800 3250
NoConn ~ 5800 3050
NoConn ~ 5800 2950
NoConn ~ 4300 1250
Wire Wire Line
	2900 5950 3200 5950
NoConn ~ 5800 3950
NoConn ~ 5800 3750
NoConn ~ 5800 3550
Wire Wire Line
	5900 3350 5800 3350
Wire Wire Line
	3050 5750 3200 5750
NoConn ~ 5800 3150
NoConn ~ 5000 1250
Wire Wire Line
	2900 5850 3200 5850
NoConn ~ 5800 4550
Wire Wire Line
	4500 1100 4500 1250
NoConn ~ 5800 5750
NoConn ~ 3800 1250
NoConn ~ 5800 6050
NoConn ~ 5800 6250
NoConn ~ 5800 4350
$Comp
L power:+3.7V #PWR?
U 1 1 5D795417
P 4600 1050
AR Path="/5D795417" Ref="#PWR?"  Part="1" 
AR Path="/5D76ED37/5D795417" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 4600 900 50  0001 C CNN
F 1 "+3.7V" H 4615 1223 50  0000 C CNN
F 2 "" H 4600 1050 50  0001 C CNN
F 3 "" H 4600 1050 50  0001 C CNN
	1    4600 1050
	1    0    0    -1  
$EndComp
$Comp
L MCU_Nordic:nRF52840 U?
U 1 1 5D79541D
P 4500 4050
AR Path="/5D79541D" Ref="U?"  Part="1" 
AR Path="/5D76ED37/5D79541D" Ref="U?"  Part="1" 
F 0 "U?" H 4500 1161 50  0000 C CNN
F 1 "nRF52840" H 4500 1070 50  0000 C CNN
F 2 "Package_DFN_QFN:Nordic_AQFN-73-1EP_7x7mm_P0.5mm" H 4500 1150 50  0001 C CNN
F 3 "http://infocenter.nordicsemi.com/topic/com.nordic.infocenter.nrf52/dita/nrf52/chips/nrf52840.html" H 3850 5950 50  0001 C CNN
	1    4500 4050
	1    0    0    -1  
$EndComp
Text Label 6450 3100 0    50   ~ 0
RXD
Text Label 6450 3450 0    50   ~ 0
TXD
Wire Wire Line
	6450 3450 6300 3450
Wire Wire Line
	6450 3100 6300 3100
$EndSCHEMATC
