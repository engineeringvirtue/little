EESchema Schematic File Version 4
EELAYER 29 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 2 3
Title "DAPLink"
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L power:+3.3V #PWR?
U 1 1 5D737590
P 3350 2200
AR Path="/5D737590" Ref="#PWR?"  Part="1" 
AR Path="/5D716E5A/5D737590" Ref="#PWR042"  Part="1" 
F 0 "#PWR042" H 3350 2050 50  0001 C CNN
F 1 "+3.3V" H 3365 2373 50  0000 C CNN
F 2 "" H 3350 2200 50  0001 C CNN
F 3 "" H 3350 2200 50  0001 C CNN
	1    3350 2200
	1    0    0    -1  
$EndComp
Connection ~ 3750 5400
Connection ~ 4050 5400
$Comp
L power:GND #PWR?
U 1 1 5D737598
P 3750 5950
AR Path="/5D737598" Ref="#PWR?"  Part="1" 
AR Path="/5D716E5A/5D737598" Ref="#PWR045"  Part="1" 
F 0 "#PWR045" H 3750 5700 50  0001 C CNN
F 1 "GND" H 3755 5777 50  0000 C CNN
F 2 "" H 3750 5950 50  0001 C CNN
F 3 "" H 3750 5950 50  0001 C CNN
	1    3750 5950
	1    0    0    -1  
$EndComp
$Comp
L Device:C C?
U 1 1 5D73759E
P 4050 5800
AR Path="/5D73759E" Ref="C?"  Part="1" 
AR Path="/5D716E5A/5D73759E" Ref="C23"  Part="1" 
F 0 "C23" H 4165 5846 50  0000 L CNN
F 1 "18pF" H 4165 5755 50  0000 L CNN
F 2 "Capacitor_SMD:C_0201_0603Metric" H 4088 5650 50  0001 C CNN
F 3 "~" H 4050 5800 50  0001 C CNN
	1    4050 5800
	1    0    0    -1  
$EndComp
$Comp
L Device:C C?
U 1 1 5D7375A4
P 3750 5800
AR Path="/5D7375A4" Ref="C?"  Part="1" 
AR Path="/5D716E5A/5D7375A4" Ref="C22"  Part="1" 
F 0 "C22" H 3636 5754 50  0000 R CNN
F 1 "18pF" H 3636 5845 50  0000 R CNN
F 2 "Capacitor_SMD:C_0201_0603Metric" H 3788 5650 50  0001 C CNN
F 3 "~" H 3750 5800 50  0001 C CNN
	1    3750 5800
	1    0    0    1   
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5D7375AA
P 4050 5950
AR Path="/5D7375AA" Ref="#PWR?"  Part="1" 
AR Path="/5D716E5A/5D7375AA" Ref="#PWR046"  Part="1" 
F 0 "#PWR046" H 4050 5700 50  0001 C CNN
F 1 "GND" H 4055 5777 50  0000 C CNN
F 2 "" H 4050 5950 50  0001 C CNN
F 3 "" H 4050 5950 50  0001 C CNN
	1    4050 5950
	1    0    0    -1  
$EndComp
Wire Wire Line
	4050 5400 4050 5650
Wire Wire Line
	3750 5400 3750 5650
$Comp
L Device:Crystal Y?
U 1 1 5D7375B2
P 3900 5400
AR Path="/5D7375B2" Ref="Y?"  Part="1" 
AR Path="/5D716E5A/5D7375B2" Ref="Y2"  Part="1" 
F 0 "Y2" H 3900 5668 50  0000 C CNN
F 1 "12MHz" H 3900 5577 50  0000 C CNN
F 2 "Crystal:Crystal_SMD_TXC_7M-4Pin_3.2x2.5mm_HandSoldering" H 3900 5400 50  0001 C CNN
F 3 "~" H 3900 5400 50  0001 C CNN
	1    3900 5400
	1    0    0    -1  
$EndComp
Wire Wire Line
	3750 5050 3750 5400
Wire Wire Line
	4050 5400 4050 5150
Connection ~ 3200 3750
Wire Wire Line
	3200 4150 3200 3750
Wire Wire Line
	2900 3350 2900 3450
Connection ~ 2900 3450
Wire Wire Line
	2900 4400 2900 4250
$Comp
L Device:R R?
U 1 1 5D7375C1
P 2900 4100
AR Path="/5D7375C1" Ref="R?"  Part="1" 
AR Path="/5D716E5A/5D7375C1" Ref="R16"  Part="1" 
F 0 "R16" H 2970 4146 50  0000 L CNN
F 1 "1.5K" H 2970 4055 50  0000 L CNN
F 2 "Resistor_SMD:R_0402_1005Metric" V 2830 4100 50  0001 C CNN
F 3 "~" H 2900 4100 50  0001 C CNN
	1    2900 4100
	1    0    0    -1  
$EndComp
$Comp
L Device:R R?
U 1 1 5D7375C7
P 3200 3600
AR Path="/5D7375C7" Ref="R?"  Part="1" 
AR Path="/5D716E5A/5D7375C7" Ref="R17"  Part="1" 
F 0 "R17" H 3270 3646 50  0000 L CNN
F 1 "10K" H 3270 3555 50  0000 L CNN
F 2 "Resistor_SMD:R_0402_1005Metric" V 3130 3600 50  0001 C CNN
F 3 "~" H 3200 3600 50  0001 C CNN
	1    3200 3600
	1    0    0    -1  
$EndComp
Wire Wire Line
	2900 3450 2900 3550
Wire Wire Line
	2900 3450 3200 3450
$Comp
L Device:Q_PMOS_DGS Q?
U 1 1 5D7375CF
P 3000 3750
AR Path="/5D7375CF" Ref="Q?"  Part="1" 
AR Path="/5D716E5A/5D7375CF" Ref="Q2"  Part="1" 
F 0 "Q2" H 3205 3704 50  0000 L CNN
F 1 "Q_PMOS_DGS" H 3205 3795 50  0000 L CNN
F 2 "Package_TO_SOT_SMD:SC-59_Handsoldering" H 3200 3850 50  0001 C CNN
F 3 "~" H 3000 3750 50  0001 C CNN
	1    3000 3750
	-1   0    0    1   
$EndComp
$Comp
L power:+3.3V #PWR?
U 1 1 5D7375D6
P 2900 3350
AR Path="/5D7375D6" Ref="#PWR?"  Part="1" 
AR Path="/5D716E5A/5D7375D6" Ref="#PWR041"  Part="1" 
F 0 "#PWR041" H 2900 3200 50  0001 C CNN
F 1 "+3.3V" H 2915 3523 50  0000 C CNN
F 2 "" H 2900 3350 50  0001 C CNN
F 3 "" H 2900 3350 50  0001 C CNN
	1    2900 3350
	1    0    0    -1  
$EndComp
Wire Wire Line
	4050 4150 3200 4150
Wire Wire Line
	4050 4400 2900 4400
$Comp
L power:GND #PWR?
U 1 1 5D7375DE
P 6950 2150
AR Path="/5D7375DE" Ref="#PWR?"  Part="1" 
AR Path="/5D716E5A/5D7375DE" Ref="#PWR048"  Part="1" 
F 0 "#PWR048" H 6950 1900 50  0001 C CNN
F 1 "GND" H 6955 1977 50  0000 C CNN
F 2 "" H 6950 2150 50  0001 C CNN
F 3 "" H 6950 2150 50  0001 C CNN
	1    6950 2150
	1    0    0    -1  
$EndComp
$Comp
L Device:C C?
U 1 1 5D7375E4
P 6950 2000
AR Path="/5D7375E4" Ref="C?"  Part="1" 
AR Path="/5D716E5A/5D7375E4" Ref="C24"  Part="1" 
F 0 "C24" H 6835 2046 50  0000 R CNN
F 1 "0.1uF" H 6835 1955 50  0000 R CNN
F 2 "Capacitor_SMD:C_0201_0603Metric" H 6988 1850 50  0001 C CNN
F 3 "~" H 6950 2000 50  0001 C CNN
	1    6950 2000
	-1   0    0    -1  
$EndComp
Wire Wire Line
	6950 1300 6950 1450
$Comp
L Device:R R?
U 1 1 5D7375EB
P 6950 1600
AR Path="/5D7375EB" Ref="R?"  Part="1" 
AR Path="/5D716E5A/5D7375EB" Ref="R19"  Part="1" 
F 0 "R19" H 7020 1646 50  0000 L CNN
F 1 "10K" H 7020 1555 50  0000 L CNN
F 2 "Resistor_SMD:R_0402_1005Metric" V 6880 1600 50  0001 C CNN
F 3 "~" H 6950 1600 50  0001 C CNN
	1    6950 1600
	1    0    0    -1  
$EndComp
Connection ~ 3700 2650
Wire Wire Line
	3900 2650 3700 2650
Wire Wire Line
	3900 2600 3900 2650
Wire Wire Line
	3350 2650 3700 2650
$Comp
L power:GND #PWR?
U 1 1 5D7375F5
P 3700 2650
AR Path="/5D7375F5" Ref="#PWR?"  Part="1" 
AR Path="/5D716E5A/5D7375F5" Ref="#PWR044"  Part="1" 
F 0 "#PWR044" H 3700 2400 50  0001 C CNN
F 1 "GND" H 3705 2477 50  0000 C CNN
F 2 "" H 3700 2650 50  0001 C CNN
F 3 "" H 3700 2650 50  0001 C CNN
	1    3700 2650
	1    0    0    -1  
$EndComp
Wire Wire Line
	3900 2350 3350 2350
Connection ~ 3350 2350
Wire Wire Line
	3350 2200 3350 2350
$Comp
L Device:C C?
U 1 1 5D7375FE
P 3350 2500
AR Path="/5D7375FE" Ref="C?"  Part="1" 
AR Path="/5D716E5A/5D7375FE" Ref="C21"  Part="1" 
F 0 "C21" H 3235 2546 50  0000 R CNN
F 1 "0.1uF" H 3235 2455 50  0000 R CNN
F 2 "Capacitor_SMD:C_0201_0603Metric" H 3388 2350 50  0001 C CNN
F 3 "~" H 3350 2500 50  0001 C CNN
	1    3350 2500
	-1   0    0    -1  
$EndComp
Wire Wire Line
	6950 1750 6950 1800
Wire Wire Line
	6950 1800 6600 1800
Connection ~ 6950 1800
Wire Wire Line
	6950 1800 6950 1850
Wire Wire Line
	6600 1800 6600 2350
Wire Wire Line
	7350 2550 7350 2650
Wire Wire Line
	3900 2350 3900 2450
Connection ~ 3900 2350
Wire Wire Line
	7350 2550 7350 2450
Connection ~ 7350 2550
$Comp
L power:+3.3V #PWR?
U 1 1 5D73760E
P 6950 1300
AR Path="/5D73760E" Ref="#PWR?"  Part="1" 
AR Path="/5D716E5A/5D73760E" Ref="#PWR047"  Part="1" 
F 0 "#PWR047" H 6950 1150 50  0001 C CNN
F 1 "+3.3V" H 6965 1473 50  0000 C CNN
F 2 "" H 6950 1300 50  0001 C CNN
F 3 "" H 6950 1300 50  0001 C CNN
	1    6950 1300
	1    0    0    -1  
$EndComp
Wire Wire Line
	4050 2350 3900 2350
Wire Wire Line
	4050 2600 3900 2600
Wire Wire Line
	3900 2450 4050 2450
Wire Wire Line
	6600 2350 6400 2350
Wire Wire Line
	6400 2550 7200 2550
$Comp
L MCU_NXP_LPC:LPC11U35 IC?
U 1 1 5D73761A
P 5200 2250
AR Path="/5D73761A" Ref="IC?"  Part="1" 
AR Path="/5D716E5A/5D73761A" Ref="IC1"  Part="1" 
F 0 "IC1" H 5200 2492 40  0000 C CNN
F 1 "LPC11U35" H 5200 2416 40  0000 C CNN
F 2 "Package_QFP:LQFP-48-1EP_7x7mm_P0.5mm_EP3.6x3.6mm" H 5200 2250 50  0001 C CNN
F 3 "" H 5200 2250 50  0001 C CNN
	1    5200 2250
	1    0    0    -1  
$EndComp
NoConn ~ 6400 4150
NoConn ~ 6400 4250
Wire Wire Line
	6400 2950 6500 2950
Wire Wire Line
	6400 3350 6500 3350
Wire Wire Line
	6400 3450 6500 3450
Wire Wire Line
	6400 3550 6500 3550
NoConn ~ 6400 4350
NoConn ~ 6400 4950
Wire Wire Line
	6400 3050 6500 3050
NoConn ~ 6400 4450
NoConn ~ 6400 4550
NoConn ~ 6400 4650
NoConn ~ 6400 4750
NoConn ~ 6400 4850
NoConn ~ 6400 5050
NoConn ~ 6400 5150
Wire Wire Line
	6400 3650 6500 3650
Wire Wire Line
	6400 3850 6500 3850
Wire Wire Line
	6400 3950 6500 3950
Wire Wire Line
	3750 5050 4050 5050
Wire Wire Line
	6400 2850 6500 2850
Wire Wire Line
	6400 3250 6500 3250
Wire Wire Line
	4050 4050 4000 4050
Text Label 6500 2850 0    50   ~ 0
LED_MSD
Text Label 6500 2950 0    50   ~ 0
LED_DAP
Text Label 6500 3050 0    50   ~ 0
LED_CDC
Wire Wire Line
	3550 4050 3650 4050
$Comp
L Switch:SW_Push SW?
U 1 1 5D73764D
P 7550 2450
AR Path="/5D73764D" Ref="SW?"  Part="1" 
AR Path="/5D716E5A/5D73764D" Ref="SW1"  Part="1" 
F 0 "SW1" H 7550 2735 50  0000 C CNN
F 1 "SW_Push" H 7550 2644 50  0000 C CNN
F 2 "Button_Switch_SMD:SW_Push_1P1T_NO_CK_KSC7xxJ" H 7550 2650 50  0001 C CNN
F 3 "~" H 7550 2650 50  0001 C CNN
	1    7550 2450
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5D737653
P 7750 2450
AR Path="/5D737653" Ref="#PWR?"  Part="1" 
AR Path="/5D716E5A/5D737653" Ref="#PWR049"  Part="1" 
F 0 "#PWR049" H 7750 2200 50  0001 C CNN
F 1 "GND" V 7755 2322 50  0000 R CNN
F 2 "" H 7750 2450 50  0001 C CNN
F 3 "" H 7750 2450 50  0001 C CNN
	1    7750 2450
	0    -1   -1   0   
$EndComp
$Comp
L Device:LED D?
U 1 1 5D745C13
P 8900 4400
AR Path="/5D745C13" Ref="D?"  Part="1" 
AR Path="/5D716E5A/5D745C13" Ref="D1"  Part="1" 
F 0 "D1" H 8893 4616 50  0000 C CNN
F 1 "LED" H 8893 4525 50  0000 C CNN
F 2 "LED_SMD:LED_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 8900 4400 50  0001 C CNN
F 3 "~" H 8900 4400 50  0001 C CNN
	1    8900 4400
	1    0    0    -1  
$EndComp
$Comp
L Device:LED D?
U 1 1 5D745C19
P 8900 4750
AR Path="/5D745C19" Ref="D?"  Part="1" 
AR Path="/5D716E5A/5D745C19" Ref="D2"  Part="1" 
F 0 "D2" H 8893 4966 50  0000 C CNN
F 1 "LED" H 8893 4875 50  0000 C CNN
F 2 "LED_SMD:LED_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 8900 4750 50  0001 C CNN
F 3 "~" H 8900 4750 50  0001 C CNN
	1    8900 4750
	1    0    0    -1  
$EndComp
$Comp
L Device:LED D?
U 1 1 5D745C1F
P 8900 5150
AR Path="/5D745C1F" Ref="D?"  Part="1" 
AR Path="/5D716E5A/5D745C1F" Ref="D3"  Part="1" 
F 0 "D3" H 8893 5366 50  0000 C CNN
F 1 "LED" H 8893 5275 50  0000 C CNN
F 2 "LED_SMD:LED_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 8900 5150 50  0001 C CNN
F 3 "~" H 8900 5150 50  0001 C CNN
	1    8900 5150
	1    0    0    -1  
$EndComp
Wire Wire Line
	9200 4150 9200 4400
Wire Wire Line
	9200 4400 9050 4400
Wire Wire Line
	9200 4400 9200 4750
Wire Wire Line
	9200 4750 9050 4750
Connection ~ 9200 4400
Wire Wire Line
	9200 4750 9200 5150
Wire Wire Line
	9200 5150 9050 5150
Connection ~ 9200 4750
Wire Wire Line
	8750 5150 8600 5150
Wire Wire Line
	8750 4750 8600 4750
Wire Wire Line
	8750 4400 8600 4400
Text Label 8300 4400 2    50   ~ 0
LED_MSD
Text Label 8300 4750 2    50   ~ 0
LED_DAP
Text Label 8300 5150 2    50   ~ 0
LED_CDC
$Comp
L Device:R R?
U 1 1 5D745C33
P 8450 4750
AR Path="/5D745C33" Ref="R?"  Part="1" 
AR Path="/5D716E5A/5D745C33" Ref="R21"  Part="1" 
F 0 "R21" V 8243 4750 50  0000 C CNN
F 1 "470" V 8334 4750 50  0000 C CNN
F 2 "Resistor_SMD:R_0402_1005Metric" V 8380 4750 50  0001 C CNN
F 3 "~" H 8450 4750 50  0001 C CNN
	1    8450 4750
	0    1    1    0   
$EndComp
$Comp
L Device:R R?
U 1 1 5D745C39
P 8450 5150
AR Path="/5D745C39" Ref="R?"  Part="1" 
AR Path="/5D716E5A/5D745C39" Ref="R22"  Part="1" 
F 0 "R22" V 8243 5150 50  0000 C CNN
F 1 "470" V 8334 5150 50  0000 C CNN
F 2 "Resistor_SMD:R_0402_1005Metric" V 8380 5150 50  0001 C CNN
F 3 "~" H 8450 5150 50  0001 C CNN
	1    8450 5150
	0    1    1    0   
$EndComp
$Comp
L Device:R R?
U 1 1 5D745C3F
P 8450 4400
AR Path="/5D745C3F" Ref="R?"  Part="1" 
AR Path="/5D716E5A/5D745C3F" Ref="R20"  Part="1" 
F 0 "R20" V 8243 4400 50  0000 C CNN
F 1 "470" V 8334 4400 50  0000 C CNN
F 2 "Resistor_SMD:R_0402_1005Metric" V 8380 4400 50  0001 C CNN
F 3 "~" H 8450 4400 50  0001 C CNN
	1    8450 4400
	0    1    1    0   
$EndComp
$Comp
L power:+3.3V #PWR?
U 1 1 5D745C45
P 9200 4150
AR Path="/5D745C45" Ref="#PWR?"  Part="1" 
AR Path="/5D716E5A/5D745C45" Ref="#PWR050"  Part="1" 
F 0 "#PWR050" H 9200 4000 50  0001 C CNN
F 1 "+3.3V" H 9215 4323 50  0000 C CNN
F 2 "" H 9200 4150 50  0001 C CNN
F 3 "" H 9200 4150 50  0001 C CNN
	1    9200 4150
	1    0    0    -1  
$EndComp
Text HLabel 6500 3250 2    50   Output ~ 0
NRESET
Text HLabel 6500 3350 2    50   Output ~ 0
SWCLK
Text HLabel 6500 3450 2    50   Output ~ 0
SWDIO
Text HLabel 6500 3550 2    50   Output ~ 0
SWO
Text HLabel 6500 3650 2    50   Output ~ 0
TDI
Text HLabel 6500 3850 2    50   Output ~ 0
TXD
Text HLabel 6500 3950 2    50   Output ~ 0
RXD
Text HLabel 3950 4300 0    50   Output ~ 0
D-
Text HLabel 2750 4400 0    50   Output ~ 0
D+
Connection ~ 2900 4400
Wire Wire Line
	2900 4400 2750 4400
Wire Wire Line
	3950 4300 4050 4300
$Comp
L Device:R R?
U 1 1 5D737645
P 3800 4050
AR Path="/5D737645" Ref="R?"  Part="1" 
AR Path="/5D716E5A/5D737645" Ref="R18"  Part="1" 
F 0 "R18" V 3593 4050 50  0000 C CNN
F 1 "10K" V 3684 4050 50  0000 C CNN
F 2 "Resistor_SMD:R_0402_1005Metric" V 3730 4050 50  0001 C CNN
F 3 "~" H 3800 4050 50  0001 C CNN
	1    3800 4050
	0    1    1    0   
$EndComp
$Comp
L Connector:TestPoint TP1
U 1 1 5D337583
P 4000 3950
F 0 "TP1" H 4058 4068 50  0000 L CNN
F 1 "TestPoint" H 4058 3977 50  0000 L CNN
F 2 "TestPoint:TestPoint_Plated_Hole_D3.0mm" H 4200 3950 50  0001 C CNN
F 3 "~" H 4200 3950 50  0001 C CNN
	1    4000 3950
	1    0    0    -1  
$EndComp
$Comp
L Connector:TestPoint TP2
U 1 1 5D3394E7
P 7200 2550
F 0 "TP2" H 7258 2668 50  0000 L CNN
F 1 "TestPoint" H 7258 2577 50  0000 L CNN
F 2 "TestPoint:TestPoint_Plated_Hole_D3.0mm" H 7400 2550 50  0001 C CNN
F 3 "~" H 7400 2550 50  0001 C CNN
	1    7200 2550
	1    0    0    -1  
$EndComp
Connection ~ 7200 2550
Wire Wire Line
	7200 2550 7350 2550
$Comp
L power:+3.3V #PWR?
U 1 1 5D3808D4
P 3550 4050
AR Path="/5D3808D4" Ref="#PWR?"  Part="1" 
AR Path="/5D716E5A/5D3808D4" Ref="#PWR043"  Part="1" 
F 0 "#PWR043" H 3550 3900 50  0001 C CNN
F 1 "+3.3V" H 3565 4223 50  0000 C CNN
F 2 "" H 3550 4050 50  0001 C CNN
F 3 "" H 3550 4050 50  0001 C CNN
	1    3550 4050
	1    0    0    -1  
$EndComp
Wire Wire Line
	6400 2650 7350 2650
Wire Wire Line
	4000 3950 4000 4050
Connection ~ 4000 4050
Wire Wire Line
	4000 4050 3950 4050
$EndSCHEMATC
