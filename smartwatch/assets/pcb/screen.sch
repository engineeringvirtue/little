EESchema Schematic File Version 4
EELAYER 29 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 3 3
Title "Display"
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L power:+13V #PWR?
U 1 1 5DB7404D
P 4200 1650
AR Path="/5DB7404D" Ref="#PWR?"  Part="1" 
AR Path="/5DB72E49/5DB7404D" Ref="#PWR037"  Part="1" 
F 0 "#PWR037" H 4200 1500 50  0001 C CNN
F 1 "+13V" H 4215 1823 50  0000 C CNN
F 2 "" H 4200 1650 50  0001 C CNN
F 3 "" H 4200 1650 50  0001 C CNN
	1    4200 1650
	1    0    0    -1  
$EndComp
NoConn ~ 5450 1700
$Comp
L Device:R R14
U 1 1 5DB774DA
P 4900 2550
F 0 "R14" H 4970 2596 50  0000 L CNN
F 1 "5.1K" H 4970 2505 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" V 4830 2550 50  0001 C CNN
F 3 "~" H 4900 2550 50  0001 C CNN
	1    4900 2550
	1    0    0    -1  
$EndComp
NoConn ~ 5450 5750
Connection ~ 4200 2000
Wire Wire Line
	4200 2000 4200 1650
$Comp
L Device:C C19
U 1 1 5DB82478
P 4700 2150
F 0 "C19" H 4815 2196 50  0000 L CNN
F 1 "0.1uF" H 4815 2105 50  0000 L CNN
F 2 "Capacitor_SMD:C_0201_0603Metric" H 4738 2000 50  0001 C CNN
F 3 "~" H 4700 2150 50  0001 C CNN
	1    4700 2150
	1    0    0    -1  
$EndComp
Connection ~ 4700 2000
Wire Wire Line
	4700 2000 4200 2000
Wire Wire Line
	5200 2550 5200 2700
Wire Wire Line
	5200 2700 4900 2700
Wire Wire Line
	4200 4950 4200 5500
Wire Wire Line
	4200 5500 4450 5500
Connection ~ 4200 4950
$Comp
L power:GND #PWR039
U 1 1 5DB9543B
P 4700 2300
F 0 "#PWR039" H 4700 2050 50  0001 C CNN
F 1 "GND" H 4705 2127 50  0000 C CNN
F 2 "" H 4700 2300 50  0001 C CNN
F 3 "" H 4700 2300 50  0001 C CNN
	1    4700 2300
	1    0    0    -1  
$EndComp
Connection ~ 4700 2300
$Comp
L power:GND #PWR038
U 1 1 5DB95B58
P 4450 5800
F 0 "#PWR038" H 4450 5550 50  0001 C CNN
F 1 "GND" H 4455 5627 50  0000 C CNN
F 2 "" H 4450 5800 50  0001 C CNN
F 3 "" H 4450 5800 50  0001 C CNN
	1    4450 5800
	1    0    0    -1  
$EndComp
$Comp
L Device:C C18
U 1 1 5DB965CD
P 4450 5650
F 0 "C18" H 4565 5696 50  0000 L CNN
F 1 "0.1uF" H 4565 5605 50  0000 L CNN
F 2 "Capacitor_SMD:C_0201_0603Metric" H 4488 5500 50  0001 C CNN
F 3 "~" H 4450 5650 50  0001 C CNN
	1    4450 5650
	1    0    0    -1  
$EndComp
Connection ~ 4450 5500
Wire Wire Line
	4450 5800 4900 5800
Connection ~ 4450 5800
$Comp
L Device:C C17
U 1 1 5DBAEDD2
P 3750 5100
F 0 "C17" H 3865 5146 50  0000 L CNN
F 1 "0.1uF" H 3865 5055 50  0000 L CNN
F 2 "Capacitor_SMD:C_0201_0603Metric" H 3788 4950 50  0001 C CNN
F 3 "~" H 3750 5100 50  0001 C CNN
	1    3750 5100
	1    0    0    -1  
$EndComp
Connection ~ 3750 5250
Wire Wire Line
	3750 5250 3450 5250
$Comp
L power:GND #PWR036
U 1 1 5DBB1A93
P 3750 4950
F 0 "#PWR036" H 3750 4700 50  0001 C CNN
F 1 "GND" H 3755 4777 50  0000 C CNN
F 2 "" H 3750 4950 50  0001 C CNN
F 3 "" H 3750 4950 50  0001 C CNN
	1    3750 4950
	-1   0    0    1   
$EndComp
Wire Wire Line
	3750 4950 4100 4950
Wire Wire Line
	4100 4950 4100 5050
Connection ~ 3750 4950
NoConn ~ 5450 4850
NoConn ~ 5450 4750
NoConn ~ 5450 3850
NoConn ~ 5450 3950
NoConn ~ 5450 4050
NoConn ~ 5450 4150
NoConn ~ 5450 4250
NoConn ~ 5450 4350
NoConn ~ 5450 3250
NoConn ~ 5450 3150
NoConn ~ 5450 3050
NoConn ~ 5450 2950
NoConn ~ 5450 2850
NoConn ~ 5450 2750
Wire Wire Line
	4450 5500 5450 5500
Wire Wire Line
	4900 5350 5450 5350
Wire Wire Line
	5450 5250 3750 5250
Wire Wire Line
	4100 5050 5450 5050
Wire Wire Line
	5450 4950 4200 4950
Wire Wire Line
	4900 3650 5450 3650
Wire Wire Line
	4900 3550 5450 3550
Wire Wire Line
	4450 3350 5450 3350
Wire Wire Line
	5450 2550 5200 2550
Wire Wire Line
	4900 2400 5450 2400
Wire Wire Line
	5450 2000 4700 2000
$Comp
L Display_Graphic:NHD-1.45-160128G U?
U 1 1 5DB74053
P 6000 3400
AR Path="/5DB74053" Ref="U?"  Part="1" 
AR Path="/5DB72E49/5DB74053" Ref="U6"  Part="1" 
F 0 "U6" H 6478 3121 50  0000 L CNN
F 1 "NHD-1.45-160128G" H 6478 3030 50  0000 L CNN
F 2 "Connector_FFC-FPC:Molex_541043531" H 6000 5300 50  0001 C CNN
F 3 "" H 6000 5300 50  0001 C CNN
	1    6000 3400
	1    0    0    -1  
$EndComp
Wire Wire Line
	5450 2650 5300 2650
Wire Wire Line
	5300 2650 5300 2800
Text HLabel 4900 2800 0    50   Output ~ 0
VDDIO
Text HLabel 3450 5250 0    50   Output ~ 0
VDD
Text HLabel 4900 3550 0    50   Output ~ 0
SCL
Text HLabel 4900 3650 0    50   Output ~ 0
SDI
NoConn ~ 5450 3750
Wire Wire Line
	5450 4550 4900 4550
Wire Wire Line
	5450 4650 4900 4650
Text HLabel 4900 4550 0    50   Output ~ 0
RS
Text HLabel 4900 4650 0    50   Output ~ 0
CSB
Wire Wire Line
	4900 5800 4900 5600
Wire Wire Line
	4900 2800 5050 2800
$Comp
L power:GND #PWR040
U 1 1 5DCB4459
P 5050 3100
F 0 "#PWR040" H 5050 2850 50  0001 C CNN
F 1 "GND" H 5055 2927 50  0000 C CNN
F 2 "" H 5050 3100 50  0001 C CNN
F 3 "" H 5050 3100 50  0001 C CNN
	1    5050 3100
	1    0    0    -1  
$EndComp
$Comp
L Device:C C20
U 1 1 5DCB445F
P 5050 2950
F 0 "C20" H 5165 2996 50  0000 L CNN
F 1 "0.1uF" H 5165 2905 50  0000 L CNN
F 2 "Capacitor_SMD:C_0201_0603Metric" H 5088 2800 50  0001 C CNN
F 3 "~" H 5050 2950 50  0001 C CNN
	1    5050 2950
	1    0    0    -1  
$EndComp
Connection ~ 5050 2800
Wire Wire Line
	5050 2800 5300 2800
$Comp
L Device:R R15
U 1 1 5DB843F0
P 5100 2000
F 0 "R15" V 4893 2000 50  0000 C CNN
F 1 "68K" V 4984 2000 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" V 5030 2000 50  0001 C CNN
F 3 "~" H 5100 2000 50  0001 C CNN
	1    5100 2000
	-1   0    0    1   
$EndComp
Connection ~ 5100 2300
Wire Wire Line
	5450 2300 5100 2300
Wire Wire Line
	5100 2300 5100 2150
Wire Wire Line
	5100 2150 5450 2150
Wire Wire Line
	4700 2300 5100 2300
$Comp
L power:PWR_FLAG #FLG07
U 1 1 5DD5BA2E
P 5100 1850
F 0 "#FLG07" H 5100 1925 50  0001 C CNN
F 1 "PWR_FLAG" V 5100 1977 50  0000 L CNN
F 2 "" H 5100 1850 50  0001 C CNN
F 3 "~" H 5100 1850 50  0001 C CNN
	1    5100 1850
	1    0    0    -1  
$EndComp
Connection ~ 5100 2150
Wire Wire Line
	5100 1850 5450 1850
Connection ~ 5100 1850
Wire Wire Line
	5450 5600 4900 5600
Connection ~ 4900 5600
Wire Wire Line
	4900 5600 4900 5350
Wire Wire Line
	4450 3350 4450 3450
$Comp
L power:GND #PWR0107
U 1 1 5D4B1B15
P 4450 3450
F 0 "#PWR0107" H 4450 3200 50  0001 C CNN
F 1 "GND" H 4455 3277 50  0000 C CNN
F 2 "" H 4450 3450 50  0001 C CNN
F 3 "" H 4450 3450 50  0001 C CNN
	1    4450 3450
	1    0    0    -1  
$EndComp
$Comp
L power:+13V #PWR?
U 1 1 5D77DB3B
P 4200 6550
AR Path="/5D77DB3B" Ref="#PWR?"  Part="1" 
AR Path="/5DB72E49/5D77DB3B" Ref="#PWR0114"  Part="1" 
F 0 "#PWR0114" H 4200 6400 50  0001 C CNN
F 1 "+13V" H 4215 6723 50  0000 C CNN
F 2 "" H 4200 6550 50  0001 C CNN
F 3 "" H 4200 6550 50  0001 C CNN
	1    4200 6550
	1    0    0    -1  
$EndComp
$Comp
L Regulator_Switching:TLV61046ADB U?
U 1 1 5D77DB41
P 2900 6650
AR Path="/5D77DB41" Ref="U?"  Part="1" 
AR Path="/5DB72E49/5D77DB41" Ref="U7"  Part="1" 
F 0 "U7" H 2900 7075 50  0000 C CNN
F 1 "TLV61046ADB" H 2900 6984 50  0000 C CNN
F 2 "Package_TO_SOT_SMD:SOT-23-6" H 2950 6500 50  0001 L CIN
F 3 "http://www.ti.com/lit/ds/symlink/tlv61046a.pdf" H 2900 6750 50  0001 C CNN
	1    2900 6650
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5D77DB47
P 2900 6850
AR Path="/5D77DB47" Ref="#PWR?"  Part="1" 
AR Path="/5DB72E49/5D77DB47" Ref="#PWR0115"  Part="1" 
F 0 "#PWR0115" H 2900 6600 50  0001 C CNN
F 1 "GND" V 2905 6722 50  0000 R CNN
F 2 "" H 2900 6850 50  0001 C CNN
F 3 "" H 2900 6850 50  0001 C CNN
	1    2900 6850
	1    0    0    -1  
$EndComp
$Comp
L Device:R R?
U 1 1 5D77DB4D
P 3500 7100
AR Path="/5D77DB4D" Ref="R?"  Part="1" 
AR Path="/5DB72E49/5D77DB4D" Ref="R3"  Part="1" 
F 0 "R3" H 3430 7054 50  0000 R CNN
F 1 "71.5K" H 3430 7145 50  0000 R CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" V 3430 7100 50  0001 C CNN
F 3 "~" H 3500 7100 50  0001 C CNN
	1    3500 7100
	-1   0    0    1   
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5D77DB53
P 3500 7250
AR Path="/5D77DB53" Ref="#PWR?"  Part="1" 
AR Path="/5DB72E49/5D77DB53" Ref="#PWR0116"  Part="1" 
F 0 "#PWR0116" H 3500 7000 50  0001 C CNN
F 1 "GND" H 3505 7077 50  0000 C CNN
F 2 "" H 3500 7250 50  0001 C CNN
F 3 "" H 3500 7250 50  0001 C CNN
	1    3500 7250
	1    0    0    -1  
$EndComp
$Comp
L Device:R R?
U 1 1 5D77DB59
P 3500 6700
AR Path="/5D77DB59" Ref="R?"  Part="1" 
AR Path="/5DB72E49/5D77DB59" Ref="R2"  Part="1" 
F 0 "R2" H 3570 6746 50  0000 L CNN
F 1 "1.1M" H 3570 6655 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" V 3430 6700 50  0001 C CNN
F 3 "~" H 3500 6700 50  0001 C CNN
	1    3500 6700
	1    0    0    -1  
$EndComp
$Comp
L Device:C C?
U 1 1 5D77DB5F
P 1950 6600
AR Path="/5D77DB5F" Ref="C?"  Part="1" 
AR Path="/5DB72E49/5D77DB5F" Ref="C12"  Part="1" 
F 0 "C12" H 2065 6646 50  0000 L CNN
F 1 "0.1uF" H 2065 6555 50  0000 L CNN
F 2 "Capacitor_SMD:C_0201_0603Metric" H 1988 6450 50  0001 C CNN
F 3 "~" H 1950 6600 50  0001 C CNN
	1    1950 6600
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5D77DB65
P 1950 6750
AR Path="/5D77DB65" Ref="#PWR?"  Part="1" 
AR Path="/5DB72E49/5D77DB65" Ref="#PWR0117"  Part="1" 
F 0 "#PWR0117" H 1950 6500 50  0001 C CNN
F 1 "GND" H 1955 6577 50  0000 C CNN
F 2 "" H 1950 6750 50  0001 C CNN
F 3 "" H 1950 6750 50  0001 C CNN
	1    1950 6750
	1    0    0    -1  
$EndComp
$Comp
L Device:C C?
U 1 1 5D77DB6B
P 4050 6700
AR Path="/5D77DB6B" Ref="C?"  Part="1" 
AR Path="/5DB72E49/5D77DB6B" Ref="C13"  Part="1" 
F 0 "C13" H 4165 6746 50  0000 L CNN
F 1 "10uF" H 4165 6655 50  0000 L CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 4088 6550 50  0001 C CNN
F 3 "~" H 4050 6700 50  0001 C CNN
	1    4050 6700
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5D77DB71
P 4050 6850
AR Path="/5D77DB71" Ref="#PWR?"  Part="1" 
AR Path="/5DB72E49/5D77DB71" Ref="#PWR0118"  Part="1" 
F 0 "#PWR0118" H 4050 6600 50  0001 C CNN
F 1 "GND" H 4055 6677 50  0000 C CNN
F 2 "" H 4050 6850 50  0001 C CNN
F 3 "" H 4050 6850 50  0001 C CNN
	1    4050 6850
	1    0    0    -1  
$EndComp
Wire Wire Line
	3400 6450 3400 6050
Wire Wire Line
	3200 6450 3400 6450
Connection ~ 3500 6550
Wire Wire Line
	3500 6550 4050 6550
Wire Wire Line
	3500 6900 3500 6950
Connection ~ 3500 6900
Wire Wire Line
	3350 6900 3500 6900
Wire Wire Line
	3350 6650 3350 6900
Wire Wire Line
	3200 6650 3350 6650
Wire Wire Line
	3500 6850 3500 6900
Wire Wire Line
	3200 6550 3500 6550
Wire Wire Line
	2550 6650 2600 6650
Wire Wire Line
	2550 6450 2550 6650
Wire Wire Line
	2550 6050 2550 6450
Connection ~ 2550 6450
Wire Wire Line
	2550 6450 2600 6450
Connection ~ 4050 6550
Wire Wire Line
	4050 6550 4200 6550
Connection ~ 1950 6450
Wire Wire Line
	3100 6050 3400 6050
Wire Wire Line
	2550 6050 2800 6050
$Comp
L Device:L L?
U 1 1 5D77DB8C
P 2950 6050
AR Path="/5D77DB8C" Ref="L?"  Part="1" 
AR Path="/5DB72E49/5D77DB8C" Ref="L4"  Part="1" 
F 0 "L4" V 3140 6050 50  0000 C CNN
F 1 "10uH" V 3049 6050 50  0000 C CNN
F 2 "Inductor_SMD:L_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 2950 6050 50  0001 C CNN
F 3 "~" H 2950 6050 50  0001 C CNN
	1    2950 6050
	0    -1   -1   0   
$EndComp
$Comp
L power:PWR_FLAG #FLG?
U 1 1 5D77DB92
P 3400 6050
AR Path="/5D77DB92" Ref="#FLG?"  Part="1" 
AR Path="/5DB72E49/5D77DB92" Ref="#FLG0101"  Part="1" 
F 0 "#FLG0101" H 3400 6125 50  0001 C CNN
F 1 "PWR_FLAG" H 3400 6223 50  0000 C CNN
F 2 "" H 3400 6050 50  0001 C CNN
F 3 "~" H 3400 6050 50  0001 C CNN
	1    3400 6050
	1    0    0    -1  
$EndComp
Connection ~ 3400 6050
Wire Wire Line
	1950 6450 2550 6450
$Comp
L power:PWR_FLAG #FLG?
U 1 1 5D77DB9A
P 1950 6450
AR Path="/5D77DB9A" Ref="#FLG?"  Part="1" 
AR Path="/5DB72E49/5D77DB9A" Ref="#FLG0102"  Part="1" 
F 0 "#FLG0102" H 1950 6525 50  0001 C CNN
F 1 "PWR_FLAG" V 1950 6577 50  0000 L CNN
F 2 "" H 1950 6450 50  0001 C CNN
F 3 "~" H 1950 6450 50  0001 C CNN
	1    1950 6450
	0    -1   -1   0   
$EndComp
$Comp
L Device:Q_NMOS_DGS Q?
U 1 1 5D77DBA0
P 1850 6250
AR Path="/5D77DBA0" Ref="Q?"  Part="1" 
AR Path="/5DB72E49/5D77DBA0" Ref="Q1"  Part="1" 
F 0 "Q1" H 2056 6296 50  0000 L CNN
F 1 "Q_NMOS_DGS" H 2056 6205 50  0000 L CNN
F 2 "Package_TO_SOT_SMD:SC-59_Handsoldering" H 2050 6350 50  0001 C CNN
F 3 "~" H 1850 6250 50  0001 C CNN
	1    1850 6250
	1    0    0    -1  
$EndComp
Wire Wire Line
	1950 6050 1950 5950
$Comp
L power:VDD #PWR0119
U 1 1 5D781204
P 1950 5950
F 0 "#PWR0119" H 1950 5800 50  0001 C CNN
F 1 "VDD" H 1967 6123 50  0000 C CNN
F 2 "" H 1950 5950 50  0001 C CNN
F 3 "" H 1950 5950 50  0001 C CNN
	1    1950 5950
	1    0    0    -1  
$EndComp
Wire Wire Line
	1650 6250 1450 6250
Text HLabel 1450 6250 0    50   Input ~ 0
SCREEN
Wire Wire Line
	4200 2000 4200 4950
$EndSCHEMATC
